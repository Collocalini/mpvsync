
{-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}
import Data.Aeson
import Data.Aeson.Lens
--import Text.ParserCombinators.Parsec
import qualified Data.ByteString.Char8 as BC8
import qualified Data.ByteString.Lazy as B
import qualified Data.Binary.Builder as BB
import Network.Socket
import Network.Http.Client
--import Network.Http.Connection (Connection)
import System.IO.Streams (InputStream, OutputStream, stdout)
import qualified System.IO.Streams as Streams
--import GHC.Generics
import Control.Lens hiding ((.=))
import qualified Control.Lens as L --hiding (element)
import Data.Maybe
import Control.Monad
import Control.Monad.Loops
import Control.Monad.State hiding (put, get)
import qualified Control.Monad.State as St
--import System.IO
--import GHC.IO.Handle.FD
import Data.Time.Clock.POSIX
--import Data.Ratio (numerator)
import Control.Concurrent (threadDelay)
import qualified Data.Text as T
import System.Environment (getArgs)

getKodiActivePlayers = BC8.readFile "jsons/getActivePlayers.json"

getKodiPlayerPositionTime :: Int -> IO BC8.ByteString
getKodiPlayerPositionTime playerid = do
   f<- BC8.readFile "jsons/getPlayerPositionTime.json"
   return $ f & key "params" . key "playerid" . _JSON .~ playerid

getKodiPlayerPositionTimeP :: Int -> BC8.ByteString -> IO BC8.ByteString
getKodiPlayerPositionTimeP playerid f = do
   --f<- BC8.readFile "jsons/getPlayerPositionTime.json"
   return $ f & key "params" . key "playerid" . _JSON .~ playerid

parseForKodiPlayerid :: BC8.ByteString -> Maybe Int
parseForKodiPlayerid s = do
   s ^? key "result" . nth 0 . key "playerid" . _Integer >>= return.fromInteger

parseForKodiPlayerPositionTime :: BC8.ByteString -> Maybe (Int, Int, Int, Int)
parseForKodiPlayerPositionTime s = do
   time $ sequence [
       s ^? key "result" . key "time" . key "hours" . _Integer >>= return.fromInteger
      ,s ^? key "result" . key "time" . key "minutes" . _Integer >>= return.fromInteger
      ,s ^? key "result" . key "time" . key "seconds" . _Integer >>= return.fromInteger
      ,s ^? key "result" . key "time" . key "milliseconds" . _Integer >>= return.fromInteger
     ]
   where
   time (Just [_1,_2,_3,_4]) = Just (_1,_2,_3,_4)
   time _ = Nothing


parseForMpvTime_pos :: BC8.ByteString -> Maybe Float
parseForMpvTime_pos s = parseMpvResponceDataFloatWithID "gtp" s



parseMpvResponceDataFloatWithID :: String -> BC8.ByteString -> Maybe Float
parseMpvResponceDataFloatWithID _id s = do
   headMaybe $ filter isJust $ map perLine $ BC8.lines s
   where
   val (Just "success", Just _2, Just _id_)
      |_id_ == (T.pack _id) = Just $ realToFrac _2
      |otherwise = Nothing
   val _ = Nothing
   perLine s =
      val  (
             s ^? key "error" . _String
            ,s ^? key "data" . _Double
            ,s ^? key "request_id" . _String
           )
   headMaybe [] = Nothing
   headMaybe (x:_) = x


parseMpvResponceDataFloat :: BC8.ByteString -> Maybe Float
parseMpvResponceDataFloat s = do
   val (
       s ^? key "error" . _String
      ,s ^? key "data" . _Double
     )
   where
   val (Just "success", Just _2) = Just $ realToFrac _2
   val _ = Nothing

parseForAudioSpeedCorrection :: BC8.ByteString -> Maybe Float
parseForAudioSpeedCorrection s = parseMpvResponceDataFloatWithID "asc" s


data RequestLabels =
   KodiRequest
   |MpvRequest
   |IDString String
   |GetTime_posMPV
   |RedByGetTime_posMPV
   |RedByGetAudioSpeedCorrectionMPV
      deriving (Show, Eq)


data RequestDistinctions = RequestDistinctions
   {
    _requestDistinctionLabels :: [RequestLabels]

   }
      deriving (Show, Eq)

makeLenses ''RequestDistinctions


data RequestL = RequestL
   {
     _requestString :: String
    ,_responseString :: Maybe BC8.ByteString
    ,_requestTimeIssued :: Maybe POSIXTime
    ,_requestTimeSend   :: Maybe POSIXTime
    ,_responseReceived  :: Maybe POSIXTime
    ,_requestID :: RequestDistinctions

   } deriving (Show, Eq)
L.makeLenses ''RequestL


{-
data Response = Response
   {
     _responseString :: String
    ,_responseTimeReseived   :: POSIXTime
    ,_responseID :: String

   }
L.makeLenses ''Response
-}


data MPPSdesync =
    MPPSdesyncLate  Float
   |MPPSdesyncAhead Float
   |MPPSdesyncSpotOn
      deriving (Show, Eq)


data MPPSconvergenceDynamic =
    MPPSCD_convergeAccelerates Float
   |MPPSCD_divergeAccelerates  Float
   |MPPSCD_convergeSlows Float
   |MPPSCD_divergeSlows Float
   |MPPSCD_convergeToDiverge Float
   |MPPSCD_divergeToConverge Float
   |MPPSCDstationary
      deriving (Show, Eq)

data MPPSconvergence =
    MPPSconverges Float
   |MPPSdiverges  Float
   |MPPSovershotLateToAhead Float
   |MPPSovershotAheadToLate Float
   |MPPSstationary
      deriving (Show, Eq)



data MultiplePlayerPlaybackSituation = MultiplePlayerPlaybackSituation {
    _mppsPlaybackConvergence        :: MPPSconvergence
   ,_mppsPlaybackConvergenceDynamic :: MPPSconvergenceDynamic
   ,_mppsDesync                     :: MPPSdesync
   ,_mppsSpeed                          :: Float
   ,_mppsSpeedPrev                      :: Float
-- ,
   }
L.makeLenses ''MultiplePlayerPlaybackSituation

fromMPPSdesync (MPPSdesyncLate d)  = d
fromMPPSdesync (MPPSdesyncAhead d) = d
fromMPPSdesync  MPPSdesyncSpotOn   = 0

toMPPSdesync d
   |d>0 = MPPSdesyncLate   d
   |d<0 = MPPSdesyncAhead $ -d
   |otherwise = MPPSdesyncSpotOn

toMPPSconvergence c'
   |c'<0 = MPPSdiverges c'
   |c'>0 = MPPSconverges c'
   |otherwise = MPPSstationary




fromMPPSconvergence (MPPSconverges c) = c
fromMPPSconvergence (MPPSdiverges c) = c
fromMPPSconvergence (MPPSovershotLateToAhead c) = c
fromMPPSconvergence (MPPSovershotAheadToLate c) = c
fromMPPSconvergence MPPSstationary = 0
--fromMPPSconvergence _ = 0





data A = A
   { _response :: Maybe BC8.ByteString
    ,_responseDroppable :: Maybe BC8.ByteString
   -- ,_requests :: [RequestL]
    ,_responses :: [RequestL]
    ,_playerid :: Maybe Int
    ,_adjThreshold :: Float
    ,_desync :: Float
    ,_pause :: Bool
    ,_nextWakeupTime :: POSIXTime
    ,_kodiPlayerPositionTime :: Maybe (Int, Int, Int, Int)
    ,_kodiPlayerPositionTimeF :: Maybe Float
    ,_kodiPlayerPositionTimeF_prev :: Maybe Float
    ,_mpvTime_pos :: Maybe Float
    ,_mpvTime_pos_prev :: Maybe Float
    ,_mpvTime_pos_adj :: Maybe Float
    ,_mpvPidK :: Float
    ,_mpvPidI :: Int
    ,_mpvPidIntegral:: [Float]
    ,_mpvSpeed :: Maybe Float
    ,_mpvSpeedPrev :: Maybe Float
    ,_mpvConvergeSpeedPrev :: Maybe MPPSconvergence
    ,_mpvSpeedAdjustmentAccumulator :: Maybe Float
    ,_mpvSpeedCommand :: Maybe Float
    ,_mpvSocket :: FilePath
    ,_mpvPrevDropped :: Word
    ,_kodiSocketHostname :: Hostname
    ,_kodiSocketPort :: Port
    ,_kodiConnection :: Connection
    ,_mpvConnection  :: Socket
    ,_mpvCommError :: Maybe IOError
    ,_verbose :: Int
   }
L.makeLenses ''A



blankRequest = RequestL
   {
     _requestString = ""
    ,_responseString = Nothing
    ,_requestTimeIssued = Nothing
    ,_requestTimeSend  = Nothing
    ,_responseReceived = Nothing
    ,_requestID = RequestDistinctions {
                        _requestDistinctionLabels = []
                     }

   }


getResponce :: String -> StateT A IO ()
getResponce f = do
   r<- St.get
   h <- liftIO $ readFile f

   t1<- liftIO $ getPOSIXTime


   liftIO $ send (r^.mpvConnection) h


   lrp<- liftIO $ recv (r^.mpvConnection) 4096

   t2<- liftIO $ getPOSIXTime

   L.assign responses $ (r^.responses) ++ [responseWrapper t1 t2 lrp]

   where
      responseWrapper _ _ "" = set responseString Nothing $ blankRequest

      responseWrapper t1 t2 x  =
         set requestTimeSend (Just t1)
            $ set responseReceived (Just t2)
            $ set responseString (Just $ BC8.pack x)
         --   $ set requestID (set requestDistinctionLabels )
            $ blankRequest



handleResponce :: (BC8.ByteString -> Maybe Float)
                  -> Getting (Maybe Float) A (Maybe Float)
                  -> Getting (Maybe Float) A (Maybe Float)
                  -> ASetter A A (Maybe Float) (Maybe Float)
                  -> ASetter A A (Maybe Float) (Maybe Float)
                  -> RequestLabels
                  -> StateT A IO ()
handleResponce parser
               valueG
               valueG_prev
               valueS
               valueS_prev
               label = do
   r<- St.get

   let responseWall = --BC8.unlines
                          map (\x-> fromJust $ x^.responseString)
                        $ filter keepUnvisited
                        $ filter (\x-> isJust $ x^.responseString)
                        $ r^.responses

   when (r^.verbose>0) $ liftIO $ BC8.putStrLn $ BC8.unlines $ responseWall

   let responceWithSaneCheck = --sane (r^.mpvSpeedCommand)
                            --concatMap (map parser . BC8.lines) responseWall
                            parseUntilFirstMatch responseWall
   let v = r^.valueG
   let vp= r^.valueG_prev

   L.assign valueS_prev $ whenRedo True v vp
   L.assign valueS $ last (Nothing : responceWithSaneCheck) -- $ applyTime_adj responceWithSaneCheck $ r^.mpvTime_pos_adj

   --L.assign mpvPrevDropped $ addBool (r^.mpvPrevDropped) $ isNothing responceWithSaneCheck
   let l = length responceWithSaneCheck
   L.assign
      responses
      $ (map (\(i,x)->set requestID
                      (set requestDistinctionLabels
                           (setLabelIfParsed i l x)
                           $ x^.requestID)
                      x)
             $ zip [1..] $ r^.responses)
   r<- St.get
   when (r^.verbose>2) $  liftIO $ print $ r^.valueG
   where
      parseUntilFirstMatch [] = []
      parseUntilFirstMatch (x:rest)
         |isJust p = [p]
         |otherwise = Nothing : parseUntilFirstMatch rest
         where
            p = parser x

      setLabelIfParsed _ 0 x = (x^.requestID^.requestDistinctionLabels)
      setLabelIfParsed n l x
         |n<=l = (label:(x^.requestID^.requestDistinctionLabels))
         |otherwise = (x^.requestID^.requestDistinctionLabels)

      keepUnvisited r = not $ any (eq label) $ r^.requestID^.requestDistinctionLabels
         where
            eq = (==)





decreaseWord :: Word -> Word
decreaseWord x
   |x==0 = x
   |otherwise = x-1


getTime_posMPV :: StateT A IO ()
getTime_posMPV = do

   r<- St.get

   when (r^.verbose>2) $  liftIO $ putStrLn "getTime_posMPV"

   getResponce "jsons/getTime_posMPV.json"

   handleResponce parseForMpvTime_pos
                  mpvTime_pos
                  mpvTime_pos_prev
                  mpvTime_pos
                  mpvTime_pos_prev
                  RedByGetTime_posMPV

         --L.assign response Nothing

   where

   sane (Just command) (Just responce)
      |inRange 0.01 command responce = Nothing
      |otherwise = Just responce
   sane _ _ = Nothing

whenRedo  :: Bool -> Maybe a -> Maybe a -> Maybe a
whenRedo True c@(Just current) _        = c
whenRedo True c@(Nothing)      previous = previous
whenRedo _    c                _        = c



addBool :: Num a => a -> Bool -> a
addBool x b
   |b = x+1
   |otherwise = x


assignIfNotNothing x y = case y of
                           Nothing -> return ()
                           Just _  -> L.assign x y


applyTime_adj (Just x) (Just y) = Just $ x+y
applyTime_adj (Just x) Nothing  = Just x
applyTime_adj _        _        = Nothing



getAudioSpeedCorrectionMPV :: StateT A IO ()
getAudioSpeedCorrectionMPV = do

   r<- St.get

   when (r^.verbose>2) $  liftIO $ putStrLn "getSpeedMPV"

   getResponce "jsons/getSpeedMPV.json"

   handleResponce parseForAudioSpeedCorrection
                  mpvSpeed
                  mpvSpeedPrev
                  mpvSpeed
                  mpvSpeedPrev
                  RedByGetAudioSpeedCorrectionMPV

         --L.assign response Nothing

   where

   sane (Just command) (Just responce)
      |not $ inRange 0.01 command responce = Nothing
      |otherwise = Just responce
   sane _ _ = Nothing

inRange d x y = (abs (x-y)) <= d




setTime_posMPV :: StateT A IO ()
setTime_posMPV = do
   r<- St.get
   when (r^.verbose>2) $  liftIO $ putStrLn comment

   jsonCommand <- liftIO $ readFile "jsons/setTime_posMPV.json"

   case ((r^.kodiPlayerPositionTime), (r^.mpvTime_pos)) of
      (Just (h,m,s,ms), Just mt) -> do
         let d = deltaT h m s ms mt

         when (r^.verbose>3) $  liftIO $ putStrLn $ comment
            ++ " case ((r^.kodiPlayerPositionTime), (r^.mpvTime_pos)) good"

         case (((abs d) > (r^.adjThreshold))||(r^.pause)) of
            False -> do
               when (r^.verbose>3) $  liftIO $ putStrLn $ comment
                  ++ " case ((abs d) > (r^.adjThreshold)) false"
               when ((r^.verbose)>2)
                  $ liftIO $ putStrLn $ ("delta t within range: " ++ (show d))

            True -> do
               when (r^.verbose>3) $  liftIO $ putStrLn $ comment
                  ++ " case ((abs d) > (r^.adjThreshold)) true"
               when ((r^.verbose)>2)
                  $ liftIO $ putStrLn $ ("adjusting mpv time: " ++ (show d))

               let c = jsonCommand & key "command" . nth 2 . _Double .~ (realToFrac (mt + d))
               e<- liftIO $ send (r^.mpvConnection) c

               --L.assign mpvSpeed $ Just 1
               L.assign desync 0
               L.assign mpvTime_pos (Just (realToFrac (mt + d))) -- $ applyTime_adj (Just (realToFrac (mt + d))) $ r^.mpvTime_pos_adj
               L.assign mpvConvergeSpeedPrev $ Just $ toMPPSconvergence 0

               when (r^.verbose>3) $  liftIO $ putStrLn $ comment ++ " case send: " ++ c
               when ((r^.verbose)>2)
                  $ liftIO $ putStrLn $ comment ++ " send response= " ++ show e


      err -> liftIO $ putStrLn $ ("cant set adjustment, time undefined: " ++ show err)

   where
   deltaT :: Int -> Int -> Int -> Int -> Float -> Float
   deltaT h m s ms mt = (kodiPlayerPositionTime_To_Float (h,m,s,ms)) - mt
   comment = "setTime_posMPV"

kodiPlayerPositionTime_To_Float :: (Int,Int,Int,Int) -> Float
kodiPlayerPositionTime_To_Float (h,m,s,ms) =
               (fromIntegral ((60*60*h) + (60*m) + s))
             + ((sign ms) * read ( "0." ++ (show $ abs ms)))

kodiPlayerPositionTime_To_FloatM = do
   r<-St.get
   return $ maybe Nothing f $ r^.kodiPlayerPositionTime
   where
      f = Just . kodiPlayerPositionTime_To_Float


sign d
   |d<0 = -1
   |d>0 = 1
   |otherwise = 0





figureMultiplePlayerPlaybackSituation  speed
                                       speed_prev
                                       --convergeSpeed
                                       convergeSpeed_prev
                                       desync
                                       desync_prev =
   MultiplePlayerPlaybackSituation {
       _mppsPlaybackConvergence        = convergeSpeed
      ,_mppsPlaybackConvergenceDynamic = csd convergeSpeed $ toMPPSconvergence convergeSpeed_prev
      ,_mppsDesync                     = toMPPSdesync desync
      ,_mppsSpeed                      = speed
      ,_mppsSpeedPrev                  = speed_prev
      }
   where
      convergeSpeed = cs (toMPPSdesync desync) (toMPPSdesync desync_prev)
      cs d d'
         |desyncMonotone d d' = csMonotone
         |overshottLatetoAhead d' d = MPPSovershotLateToAhead $ abs overshottDiff
         |overshottAheadToLate d' d = MPPSovershotAheadToLate $ abs overshottDiff
         |otherwise                 = MPPSstationary
         where
            desyncMonotone (MPPSdesyncLate _) (MPPSdesyncLate _) = True
            desyncMonotone (MPPSdesyncAhead _) (MPPSdesyncAhead _) = True
            desyncMonotone _ _ = False

            csMonotone
               |diff > 0 = MPPSconverges $ abs diff
               |diff < 0 = MPPSdiverges  $ abs diff
               |otherwise = MPPSstationary
               where
                  diff = (fromMPPSdesync d')- (fromMPPSdesync d)

            overshottLatetoAhead (MPPSdesyncLate _) (MPPSdesyncAhead _) = True
            overshottLatetoAhead _ _ = False

            overshottAheadToLate (MPPSdesyncAhead _) (MPPSdesyncLate _) = True
            overshottAheadToLate _ _ = False

            overshottDiff = (fromMPPSdesync d') + (fromMPPSdesync d)


      csd c c'
         |cMonotone c c' = csdMonotone
         |cConvergeToDiverge c' c = MPPSCD_convergeToDiverge $ abs overshottDiff
         |cDivergeToConverge c' c = MPPSCD_divergeToConverge $ abs overshottDiff
         where
            cMonotone (MPPSconverges _) (MPPSconverges _) = True
            cMonotone (MPPSdiverges  _) (MPPSdiverges  _) = True
            cMonotone _ _ = False

            csdMonotone
               |converges c && accelerates = MPPSCD_convergeAccelerates $ abs $ cc - cc'
               |converges c && decelerates = MPPSCD_convergeSlows       $ abs $ cc - cc'
               |diverges  c && accelerates = MPPSCD_divergeAccelerates  $ abs $ cc - cc'
               |diverges  c && decelerates = MPPSCD_divergeSlows        $ abs $ cc - cc'
               |(converges c && stationary) || (diverges c && stationary)  = MPPSCDstationary
               where
                  cc  = fromMPPSconvergence c
                  cc' = fromMPPSconvergence c'

                  converges (MPPSconverges _) = True
                  converges _ = False

                  diverges (MPPSdiverges _) = True
                  diverges _ = False

                  accelerates = cc > cc'
                  decelerates = cc < cc'
                  stationary  = cc == cc'

            cConvergeToDiverge (MPPSconverges _) (MPPSdiverges  _) = True
            cConvergeToDiverge _ _ = False

            cDivergeToConverge (MPPSdiverges  _) (MPPSconverges _) = True
            cDivergeToConverge _ _ = False

            overshottDiff = (fromMPPSconvergence c') + (fromMPPSconvergence c)


speedAdjustment'' :: MultiplePlayerPlaybackSituation -> StateT A IO Float
speedAdjustment'' mpps {-s sp cs csp d d'-}
   |isLateAndConverges  (mpps^.mppsDesync) (mpps^.mppsPlaybackConvergence) = late
   |isAheadAndConverges (mpps^.mppsDesync) (mpps^.mppsPlaybackConvergence) = ahead
   |isLateAndDiverges  (mpps^.mppsDesync) (mpps^.mppsPlaybackConvergence)  = tooSlow
   |isAheadAndDiverges (mpps^.mppsDesync) (mpps^.mppsPlaybackConvergence)  = tooFast
--   |d>0 && d'<0 = transitionBackward
--   |d<0 && d'>0 = transitionForward
   |otherwise = return $ mpps^.mppsSpeed  -- + ((sign d) * 0.01)
   where

      s   = mpps^.mppsSpeed
      sp  = mpps^.mppsSpeedPrev
      cs  = mpps^.mppsPlaybackConvergence
      d   = mpps^.mppsDesync

      isLateAndConverges (MPPSdesyncLate _) (MPPSconverges _) = True
      isLateAndConverges _ _ = False

      isAheadAndConverges (MPPSdesyncAhead _) (MPPSconverges _) = True
      isAheadAndConverges _ _ = False

      isLateAndDiverges (MPPSdesyncLate _) (MPPSdiverges _) = True
      isLateAndDiverges _ _ = False

      isAheadAndDiverges (MPPSdesyncAhead _) (MPPSdiverges _) = True
      isAheadAndDiverges _ _ = False

      late  = do
         r<- St.get
         L.assign mpvSpeedAdjustmentAccumulator $ Just $ 1/5/2
         let x = 0 + (responceCurve 1 10 $ noMoreThen0_05' s
                  $ s * ((fromJust ttcc)/xSec))
         when (r^.verbose>0) $  liftIO $ putStrLn
            $ "late d=" ++ (show d)
            --   ++ " d'=" ++ (show d')
               ++ " cs=" ++ (show cs)
            --   ++ " csp=" ++ (show csp)
               ++ " ttcc=" ++ (show $ ttcc)
            --   ++ " 5/(d/cs)=" ++ (show $ 5/(d/cs))
               ++ " sa=" ++ (show $ sa)
         return x



      {-overAccelerationPrevented
      --   |ttcc < xSec && converges = noMoreThen0_05 $ -0.01 * (abs ((ics - cs)/sadics))
      --   |ttcc < xSec && (not converges) = - 0.01
         |otherwise = abs $ noMoreThen0_05' s inXsecX
-}


   --   ics  = cs * tcsi -- increased cs
   --   tcsi = xSec/ttcc -- in what proportion cs should be increased for ttcc to reach xSec period
   --   sadics =(csp-cs) -- difference in converge speed per sa adjustment




      ahead = do
         r<- St.get
         L.assign mpvSpeedAdjustmentAccumulator $ Just $ 1/5/2
         let x = 0 + (responceCurve 1 10 $ noMoreThen0_05' s
                  $ s * (xSec/(fromJust ttcc)))
         when (r^.verbose>0) $  liftIO $ putStrLn
            $ "ahead d=" ++ (show d)
            --   ++ " d'=" ++ (show d')
               ++ " cs=" ++ (show cs)
            --   ++ " csp=" ++ (show csp)
               ++ " ttcc=" ++ (show $ ttcc)
            --   ++ " 5/(d/cs)=" ++ (show $ 5/(d/cs))
               ++ " sa=" ++ (show $ sa)
         return x

      tooFast
         |diverges cs && divergeSlows pcd = useZeroOutCS
         |otherwise = do
            r<- St.get
            L.assign mpvSpeedAdjustmentAccumulator $ Just $ noMoreThen0_05
                                    $ (fromJust $ r^.mpvSpeedAdjustmentAccumulator)
                                    + (fromJust $ r^.mpvSpeedAdjustmentAccumulator)
            let x = s - (responceCurve 1 10 $ fromJust $ r^.mpvSpeedAdjustmentAccumulator)
            when (r^.verbose>0) $  liftIO $ putStrLn
               $ "tooFast 2 d=" ++ (show d)
               --   ++ " d=" ++ (show d)
                  ++ " cs=" ++ (show cs)
               --   ++ " csp=" ++ (show csp)
                  ++ " sa=" ++ (show $ sa)
               --   ++ " cs/(csp-cs)=" ++ (show $ cs/(csp-cs))
               --   ++ " csp/cs=" ++ (show $ csp/cs)
            return x
         where
            useZeroOutCS = do
               r<- St.get
               L.assign mpvSpeedAdjustmentAccumulator $ Just $ 1/5/2
               let asa = fromJust $ r^.mpvSpeedAdjustmentAccumulator
               let x = sp - (responceCurve 1 10 $ noMoreThen0_05 $ noMoreThen0_05 $ zeroOutCS' asa)
               when (r^.verbose>0) $  liftIO $ putStrLn
                  $ "tooFast 1 d=" ++ (show d)
                  --   ++ " d=" ++ (show d)
                     ++ " cs=" ++ (show cs)
                  --   ++ " csp=" ++ (show csp)
                     ++ " sa=" ++ (show $ sa)
                  --   ++ " cs/(csp-cs)=" ++ (show $ cs/(csp-cs))
                  --   ++ " csp/cs=" ++ (show $ csp/cs)
               return x



      tooSlow
         |diverges cs && divergeSlows pcd = useZeroOutCS
         |otherwise = do
            r<- St.get
            L.assign mpvSpeedAdjustmentAccumulator $ Just $ noMoreThen0_05
                                    $ (fromJust $ r^.mpvSpeedAdjustmentAccumulator)
                                    + (fromJust $ r^.mpvSpeedAdjustmentAccumulator)
            let x = s + (responceCurve 1 10 $ noMoreThen0_05 $ fromJust $ r^.mpvSpeedAdjustmentAccumulator)
            when (r^.verbose>0) $  liftIO $ putStrLn
               $ "tooSlow 2 d=" ++ (show d)
               --   ++ " d=" ++ (show d)
                  ++ " cs=" ++ (show cs)
               --   ++ " csp=" ++ (show csp)
                  ++ " sa=" ++ (show $ sa)
               --   ++ " cs/(csp-cs)=" ++ (show $ cs/(csp-cs))
               --   ++ " csp/cs=" ++ (show $ csp/cs)

            return x
         where
            useZeroOutCS = do
               r<- St.get
               L.assign mpvSpeedAdjustmentAccumulator $ Just $ 1/5/2
               let asa = fromJust $ r^.mpvSpeedAdjustmentAccumulator
               let x = sp + (responceCurve 1 10 $ noMoreThen0_05 $ zeroOutCS' asa)
               when (r^.verbose>0) $  liftIO $ putStrLn
                  $ "tooSlow 1 d=" ++ (show d)
                  --   ++ " d=" ++ (show d)
                     ++ " cs=" ++ (show cs)
                  --   ++ " csp=" ++ (show csp)
                     ++ " sa=" ++ (show $ sa)
                  --   ++ " cs/(csp-cs)=" ++ (show $ cs/(csp-cs))
                  --   ++ " csp/cs=" ++ (show $ csp/cs)
               return x

{-}
      transitionForward
         |sa>0 = do
            r<- St.get
            when (r^.verbose>0) $  liftIO $ putStrLn
               $ "transitionForward 1 d=" ++ (show d)
               --   ++ " d'=" ++ (show d')
                  ++ " cs=" ++ (show cs)
               --   ++ " csp=" ++ (show csp)
                  ++ " sa=" ++ (show $ sp-s)
            return $ sp - sa
         |otherwise = do
               r<- St.get
               when (r^.verbose>0) $  liftIO $ putStrLn
                  $ "transitionForward 2 d=" ++ (show d)
                  --   ++ " d'=" ++ (show d')
                     ++ " cs=" ++ (show cs)
                  --   ++ " csp=" ++ (show csp)
                     ++ " sa=" ++ (show $ sp-s)
               return $ s + (responceCurve (r^.adjThreshold) dd (sa*2))
            where
               dd = ddd d
               ddd (MPPSdesyncLate  x) = x
               ddd (MPPSdesyncAhead x) = x
               ddd  MPPSdesyncSpotOn   = 0
-}

      xSec = 5
      pcd = mpps^.mppsPlaybackConvergenceDynamic
      sa = s - sp

      responceCurve _max d control
      --   |d<=_max = (abs ((d^2)/(_max^2))) * control
         |otherwise = control


      noMoreThen0_05 d
         |abs d <= 0.05 = d
         |otherwise = (sign d) * 0.05

      noMoreThen0_05' c d
         |abs (c - d) <= 0.05 = d
         |otherwise = c + ((sign (d - c)) * 0.05)

      diverges (MPPSdiverges _) = True
      diverges _ = False

      divergeSlows (MPPSCD_divergeSlows _) = True
      divergeSlows _ = False



      zeroOutCS  -- = sa*(abs (cs/(csp-cs)))
         |diverges cs
            && divergeSlows pcd
            && isDueToSpeedAdjustment d = sa*(abs (divergeSpeed/divergeSlowingValue))
         |otherwise = sa
         where


            ds (MPPSdiverges x) = Just x
            ds _ = Nothing

            divergeSpeed = fromJust $ ds cs

            divergeSlowing (MPPSCD_divergeSlows x) = Just x
            divergeSlowing _ = Nothing

            divergeSlowingValue = fromJust $ divergeSlowing pcd



            isDueToSpeedAdjustment (MPPSdesyncLate _)
               |sa>0 = True
               |otherwise = False
            isDueToSpeedAdjustment (MPPSdesyncAhead _)
               |sa<0 = True
               |otherwise = False


      zeroOutCS' asa -- = sa*(abs (cs/(csp-cs)))
         |diverges cs
            && divergeSlows pcd
            && isDueToSpeedAdjustment d = asa*(abs (divergeSpeed/divergeSlowingValue))
         |otherwise = sa
         where


            ds (MPPSdiverges x) = Just x
            ds _ = Nothing

            divergeSpeed = fromJust $ ds cs

            divergeSlowing (MPPSCD_divergeSlows x) = Just x
            divergeSlowing _ = Nothing

            divergeSlowingValue = fromJust $ divergeSlowing pcd



            isDueToSpeedAdjustment (MPPSdesyncLate _)
               |sa>0 = True
               |otherwise = False
            isDueToSpeedAdjustment (MPPSdesyncAhead _)
               |sa<0 = True
               |otherwise = False


      --inXsecX = inXsec xSec


      {-inXsec x -- = sa / abs (ttcc/x)
      --   |ttccTooSlow && (ttcc /= Nothing) = s * ((fromJust ttcc)/x)
      --   |ttccTooFast && (ttcc /= Nothing) = s * (x/(fromJust ttcc))
         |otherwise = s
            where
               ttccTooSlow
               --   |ttcc == Nothing = True
                  |(fromJust ttcc) > x = True
                  |otherwise = False

               ttccTooFast
               --   |ttcc == Nothing = False
                  |(fromJust ttcc) < x = True
                  |otherwise = False

-}

      ttccTooSlow
      --   |ttcc == Nothing = True
         |(fromJust ttcc) > xSec = True
         |otherwise = False

      ttccTooFast
      --   |ttcc == Nothing = False
         |(fromJust ttcc) < xSec = True
         |otherwise = False


      ttcc -- = abs (d/cs) --time till complete converge
         |converges cs = Just $ abs (ttcc_d/ttcc_cs)
         |otherwise = Nothing
         where
            converges (MPPSconverges _) = True
            converges _ = False

            ttcc_d = fromMPPSdesync d
            ttcc_cs = fromMPPSconvergence cs





setSpeedMPV :: StateT A IO ()
setSpeedMPV = do
   r<- St.get
   when (r^.verbose>2) $  liftIO $ putStrLn comment

   jsonCommand <- liftIO $ readFile "jsons/setSpeedMPV.json"

   case ((r^.kodiPlayerPositionTime)
       , (r^.mpvTime_pos)
       , (r^.mpvSpeed)
       , (r^.mpvSpeedPrev)
       , (r^.mpvConvergeSpeedPrev)) of
      (Just (h,m,s,ms), Just mt, Just msc, Just mscp, Just mcsp) -> do

         let d = deltaT h m s ms mt


         when (r^.verbose>3) $  liftIO $ putStrLn $ comment
            ++ " case ((r^.kodiPlayerPositionTime), (r^.mpvTime_pos)) good"

         --case True of
         case ((abs d) > 0.04) of
            False -> do
               when (r^.verbose>3) $  liftIO $ putStrLn $ comment
                  ++ " case ((abs d) > (r^.adjThreshold/3)) false"
               when ((r^.verbose)>2)
                  $ liftIO $ putStrLn $ ("delta t within range: " ++ (show d))

               L.assign mpvConvergeSpeedPrev $ Just $ toMPPSconvergence ((abs (r^.desync)) - (abs d))
               L.assign desync d

            True -> do
               when (r^.verbose>3) $  liftIO $ putStrLn $ comment
                  ++ " case ((abs d) > (r^.adjThreshold/3)) true"
               when ((r^.verbose)>2)
                  $ liftIO $ putStrLn $ ("adjusting mpv delta: " ++ (show d))


               spa<- speedAdjustment'' $ figureMultiplePlayerPlaybackSituation
                                             msc
                                             mscp
                                          --   (cs d (r^.desync))
                                             (fromMPPSconvergence mcsp)
                                             d
                                             $ r^.desync

               L.assign mpvSpeedCommand $ Just $ {-fromMaybe msc -} spa
               L.assign mpvSpeedPrev $ r^.mpvSpeed
               L.assign mpvSpeed $ Just spa
               r<- St.get

               let c = jsonCommand & key "command" . nth 2 . _Double .~
                                                      (realToFrac $ fromJust $ r^.mpvSpeedCommand)
               e<- liftIO $ send (r^.mpvConnection) c


               L.assign mpvConvergeSpeedPrev $ Just $ toMPPSconvergence ((abs (r^.desync)) - (abs d))
               L.assign desync d

               when (r^.verbose>3) $  liftIO $ putStrLn $ comment ++ " case send: " ++ c
               when ((r^.verbose)>2)
                  $ liftIO $ putStrLn $ comment ++ " send response= " ++ show e


      err -> liftIO $ putStrLn $ ("cant set adjustment, time undefined: " ++ show err)

   where
   deltaT :: Int -> Int -> Int -> Int -> Float -> Float
   deltaT h m s ms mt = (kodiPlayerPositionTime_To_Float (h,m,s,ms)) - mt

   comment = "setSpeedMPV"






{-
cs d d'
   |d>=0 && d'>=0 = d'-d
   |d<=0 && d'<=0 = bothNegative
   |d>=0 && d'<=0 = -d --diverges
   |d<=0 && d'>=0 = d  --diverges
   |otherwise = 0
   where
      bothNegative
         |d>d' = abs (d'-d) --converges |  negative<-.....d'...d...0....->positive
         |d<d' =   - (d'-d) --diverges  |  negative<-.....d....d'...0....->positive
         |otherwise = 0
-}






{-
speedAdjustment'''   :: Float
                     -> StateT A IO (Maybe Float)
speedAdjustment''' d = do
   r<-St.get

   L.assign mpvPidIntegral $ pidWriteDelta (r^.mpvPidIntegral) (r^.mpvPidI) d
   r<-St.get

   let k = r^.mpvPidK
   let i = pidGetIntegral $ r^.mpvPidIntegral
   kodi<-kodiTimeLapsed
   mpv<-mpvTimeLapsed

   let s =  (r^.mpvSpeed)
               `mult`
            (
               (kodi `plus` (Just $ (d/k) ))
               `divide`
               (mpv `plus` ((r^.mpvTime_pos_adj) ))
            )
   when ((r^.verbose)>3)
      $ liftIO $ putStrLn $ "speedAdjustment k=" ++ (show k)
                                        ++ " i=" ++ (show i)
                                        ++ " kodi=" ++ (show kodi)
                                        ++ " mpv=" ++ (show mpv)
                                        ++ " s:" ++ (show s)




   return {- $ noMoreThen 0.05 (r^.mpvSpeed)-} s
   where

      mpvTimeLapsed = do
         r<-St.get
         return $ (r^.mpvTime_pos) `minus` (r^.mpvTime_pos_prev)

      kodiTimeLapsed = do
         r<-St.get
         return $ (r^.kodiPlayerPositionTimeF) `minus` (r^.kodiPlayerPositionTimeF_prev)

      minus = maybeAction ( - )
      mult  = maybeAction ( * )
      plus  = maybeAction ( + )
      divide  = maybeAction ( / )

      maybeAction f (Just x) (Just y) = Just $ f x y
      --   |x>=y = Just $ f x y
      --   |otherwise = Nothing
      maybeAction _ _ _ = Nothing



pidGetIntegral i = s / l
   where
      s=foldl (+) 0 i
      l=fromIntegral $ length i

pidWriteDelta l i d
   |(length l)< i = (d:l)
   |otherwise = (d:(take (i-1) l))

--noMoreThen
noMoreThen max_ x y
   |abs ((abs x) - (abs y)) <= max_ = y
   |otherwise = x + (sign (y-x) * max_)
-}

requestBody b o = Streams.write (Just $ BB.fromByteString $ b)
                                o


typicalKodiJSON_request = buildRequest1 $ do
      http POST "/jsonrpc"
      --setAccept "text/html"
      setAccept "application/json"



responseHandling c j n = receiveResponse c f
   where
   f p i = do
      xm <- Streams.read i
      case xm of
         Just x    -> j x
         Nothing   -> n








kodiSidePreparation :: StateT A IO ()
kodiSidePreparation = do
   r<- St.get

   c <- liftIO $ openConnection (r^.kodiSocketHostname) (r^.kodiSocketPort)
   kodiConnection L..= c

   kodiSidePreparationGetActivePlayers





kodiSidePreparationGetActivePlayers :: StateT A IO ()
kodiSidePreparationGetActivePlayers = do
   r<- St.get
   when (r^.verbose>3) $ liftIO $ putStrLn "kodiSidePreparationGetActivePlayers"
   getKodiActivePlayers' <- liftIO getKodiActivePlayers

   liftIO $ sendRequest (r^.kodiConnection)
                        typicalKodiJSON_request
                        (requestBody getKodiActivePlayers')

   (liftIO $ responseHandling (r^.kodiConnection)
                              (return.Just )
                              (return $ Nothing)) >>= (response L..=)

   r<- St.get
   when (r^.verbose>0) $ liftIO $ BC8.putStrLn $ fromMaybe "---" $ r^.response

   case (r^.response) of
         Nothing -> do
            (liftIO $ putStrLn "no response")
            playerid L..= Nothing
         Just r  -> playerid L..= (parseForKodiPlayerid r)



sendPauseMpv :: Bool -> StateT A IO ()
sendPauseMpv p = do
   r<- St.get
   when (r^.verbose>3) $ liftIO $ putStrLn comment

   jsonCommand <- liftIO $ readFile "jsons/setPauseMPV.json"

   let c = jsonCommand & key "command" . nth 2 . _Bool .~ p
   e<- liftIO $ send (r^.mpvConnection) c

   when (r^.verbose>3) $  liftIO $ putStrLn $ comment ++ " case send: " ++ c
   when ((r^.verbose)>2)
      $ liftIO $ putStrLn $ comment ++ " send response= " ++ show e

   where
      comment = "sendPauseMpv"





kodiGetPlayerPositionTime :: Int -> StateT A IO ()
kodiGetPlayerPositionTime p = do

   --L.assign response Nothing

   r<- St.get
   when (r^.verbose>3) $ liftIO $ putStrLn "kodiGetPlayerPositionTime"
   getKodiPlayerPositionTime' <- liftIO $ getKodiPlayerPositionTime p
   --forever $ do
   liftIO $ sendRequest
      (r^.kodiConnection)
      typicalKodiJSON_request
      (requestBody $ getKodiPlayerPositionTime')

   (liftIO $ responseHandling (r^.kodiConnection)
                              (return.Just )
                              (return $ Nothing)) >>= (response L..=)

   r<- St.get
   let kppt = maybe Nothing
                    parseForKodiPlayerPositionTime
                    (r^.response)


   case (isPaused kppt $ r^.kodiPlayerPositionTime) of
      True -> do
         when (r^.verbose>0) $ liftIO $ putStrLn "is paused"
         case (r^.pause) of
            False -> do
               sendPauseMpv True
               L.assign pause True
            True -> return ()

      False -> do

         kpptf <- kodiPlayerPositionTime_To_FloatM
         L.assign kodiPlayerPositionTimeF_prev $ r^.kodiPlayerPositionTimeF
         L.assign kodiPlayerPositionTime $ maybe Nothing
                                                 parseForKodiPlayerPositionTime
                                                 (r^.response)
         r<- St.get
         L.assign kodiPlayerPositionTimeF kpptf

         when (r^.verbose>0) $ liftIO $ BC8.putStrLn $ fromMaybe "---" $ r^.response
         when (r^.verbose>0) $ liftIO $ print $ r^.kodiPlayerPositionTime

         case (r^.pause) of
            True -> do
               sendPauseMpv False
               L.assign pause False
            False -> return ()


   where
      isPaused (Just kppt) (Just kppt_prev)
         |kppt == kppt_prev = True
         |otherwise = False
      isPaused _ _ = False


mpvSidePreparation :: StateT A IO ()
mpvSidePreparation = do
   r<- St.get
   soc <- liftIO $ socket AF_UNIX Stream 0
   liftIO $ connect soc $ SockAddrUnix $ r^.mpvSocket
   mpvConnection L..= soc



mpvReopenConnection :: StateT A IO ()
mpvReopenConnection = do
   r<- St.get
   delayLoop 0.1
   liftIO $ sClose $ r^.mpvConnection
   delayLoop 0.1
   mpvSidePreparation
   delayLoop 0.1


mpvAtomicComunication :: (StateT A IO ()) -> StateT A IO ()
mpvAtomicComunication f = do
   mpvReopenConnection
   f
   mpvReopenConnection



delayLoop :: POSIXTime -> StateT A IO ()
delayLoop x = do
   r<- St.get
   t1<- liftIO $ getPOSIXTime
   L.assign nextWakeupTime (t1+x)

   let cnd = do
               t1<- liftIO $ getPOSIXTime
               return ((r^.nextWakeupTime) > t1)

   whileM_ cnd $ do

      when (r^.verbose>4) $  liftIO
         $ putStrLn $ "wakeup at "
            ++ (show $ r^.nextWakeupTime)
            ++ " now " ++ (show t1)
            ++ " remained " ++ (show (t1 - r^.nextWakeupTime))

      liftIO $ threadDelay 1







{-
issueRequest request lable = do
   r<- St.get

   t1<- liftIO $ getPOSIXTime
   L.assign requests $
        (set requestString     request
       $ set requestTimeIssued (Just t1)
       $ set requestID         lable
       $ blankRequest)
       :(r^.requests)
   return ()

-}











dispatcher :: StateT A IO ()
dispatcher = do
   r<- St.get

   --rp <- listenToMPV

   liftIO $ putStrLn "dispatcher"

   L.assign
      responses
      $ filter noParses
               $ r^.responses

   where
      noParses r = not $ and [ anyMatches RedByGetTime_posMPV
                           --   ,anyMatches RedByGetAudioSpeedCorrectionMPV
                              ]
         where
            eq = (==)
            anyMatches x = any (eq x) $ r^.requestID^.requestDistinctionLabels




loop :: StateT A IO ()
loop = do
   kodiSidePreparation
   mpvSidePreparation

   forever $ do

      r<- St.get

      case (r^.playerid) of
         Nothing -> do
            liftIO $ putStrLn "no playerid found"
            kodiSidePreparationGetActivePlayers
         Just p -> do

            t1<- liftIO $ getPOSIXTime

            case ((r^.nextWakeupTime) < t1)of
               False -> do
                  when (r^.verbose>4) $  liftIO
                     $ putStrLn $ "wakeup at "
                        ++ (show $ r^.nextWakeupTime)
                        ++ " now " ++ (show t1)
                        ++ " remained " ++ (show (t1 - r^.nextWakeupTime))

                  liftIO $ threadDelay 1000
               True -> do
                  when (r^.verbose>0) $  liftIO
                     $ putStrLn $ "\n--- New cycle ---"

                  when (r^.verbose>4) $  liftIO
                     $ putStrLn $ "wakeup at " ++ (show $ r^.nextWakeupTime) ++ " now " ++ (show t1)





                  kodiGetPlayerPositionTime p

                  t2<- liftIO $ getPOSIXTime


                  getTime_posMPV



                  t3<- liftIO $ getPOSIXTime

                  let ping = t3-t1
                  let delayTime = case (ping>0.1) of
                                    True -> ping
                                    False -> 0.1
                  r<- St.get

                  L.assign mpvTime_pos $ maybe
                     (r^.mpvTime_pos)
                     (\mt-> Just $ mt - realToFrac ping)
                     (r^.mpvTime_pos)

                  r<- St.get
                  when (r^.verbose>3) $  liftIO
                     $ putStrLn $ "ping " ++ (show ping)
                  when (r^.verbose>3) $  liftIO
                     $ putStrLn $ "mpv time with ping correction " ++ (show $ r^.mpvTime_pos)


                  setTime_posMPV


                  --getAudioSpeedCorrectionMPV

                  setSpeedMPV





                  L.assign mpvPrevDropped $ mpvResetResponceIfTriedAll 2 $ r^.mpvPrevDropped


                  kodiSidePreparationGetActivePlayers


                  dispatcher

                  t1<- liftIO $ getPOSIXTime
                  L.assign nextWakeupTime (t1+1)

   r<- St.get
   liftIO $ closeConnection $ r^.kodiConnection
   liftIO $ sClose $ r^.mpvConnection

   where
      mpvResetResponceIfTriedAll total x
         |x>=total = 0
         |otherwise = x




main :: IO ()
main = do
   a<- getArgs
   j<- readFile $ unwords a

   evalStateT loop $ A {
      _response = Nothing
     ,_responseDroppable = Nothing
     ,_playerid = Nothing
     ,_mpvSocket = fromMaybe "/tmp/mpvsocket" $ j ^? key "mpvSocket"  . _JSON
     ,_kodiSocketHostname = BC8.pack $ T.unpack $ fromMaybe "192.168.2.2"
                                                           $ j ^? key "kodiSocketHostname" . _String
     ,_kodiSocketPort = fromMaybe 8080  $ j ^? key "kodiSocketPort" . _JSON
     ,_verbose = fromMaybe 0 $ j ^? key "verbose" . _JSON
     ,_mpvCommError = Nothing
     ,_adjThreshold = fromMaybe 1 $ j ^? key "adjThreshold" . _JSON
     ,_nextWakeupTime = 0
     ,_desync = 0
     ,_pause = False
     ,_kodiPlayerPositionTime = Nothing
     ,_kodiPlayerPositionTimeF = Nothing
     ,_kodiPlayerPositionTimeF_prev = Nothing
     ,_mpvTime_pos = Nothing
     ,_mpvTime_pos_prev = Nothing
     ,_mpvSpeed = Just 1.0
     ,_mpvSpeedPrev = Just 1.0
     ,_mpvSpeedAdjustmentAccumulator = Just 0.005
     ,_mpvConvergeSpeedPrev = Just $ toMPPSconvergence 0
     ,_mpvSpeedCommand = Just 1
     ,_mpvPrevDropped = 0
     ,_mpvTime_pos_adj = Just $ fromMaybe 0 $ j ^? key "mpvTime_pos_adj" . _JSON
     ,_mpvPidK = fromMaybe 5 $ j ^? key "mpvPidK" . _JSON
     ,_mpvPidI = fromMaybe 5 $ j ^? key "mpvPidI" . _JSON
     ,_mpvPidIntegral = []
    -- ,_kodiConnection :: Connection
    -- ,_mpvConnection  :: Connection
     ,_responses = []
     }
